from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():  # capture data as it moves!
            username = request.POST.get("username")
            password = request.POST.get(
                "password"
            )  # do something more with it!
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {
        "form": form,  # first is the call in url, second is all the info
    }
    return render(request, "registration/signup.html", context)
